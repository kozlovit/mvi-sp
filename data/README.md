## Data Description  

The data file contains data sample containing article information collected via News API (https://newsapi.org/)
News API is a simple HTTP REST API for searching and retrieving live articles from all over the web.
News API indexes articles from over 30,000 worldwide sources, including ABC News, BBC News, ESPN and others.

###### Row example
{   
    "source": {  
        "id": "bbc-news",  
        "name": "BBC News"  
    },    
    "author": "BBC News",  
    "title": "'Dangerous' to retrieve missionary's body",  
    "description": "A human rights group says India should abandon efforts to retrieve the US missionary's body.",  
    "url": "http://www.bbc.co.uk/news/world-asia-india-46345231",  
    "urlToImage": "https://ichef.bbci.co.uk/images/ic/1024x576/p06swfmn.jpg",  
    "publishedAt": "2018-11-26T16:19:01Z",  
    "content": "Media caption Who are the Sentinelese? Indian officials should abandon efforts to retrieve the body of an American missionary reportedly killed by an endangered tribe in the Andaman and Nicobar islands, a rights group says. Survival International says any att… [+2741 chars]"  
}

###### Row Description

**source**
object
The identifier id and a display name name for the source this article came from.

**author**
*string*
The author of the article

**title**
*string*
The headline or title of the article.

**description**
*string*
A description or snippet from the article.

**url**
*string*
The direct URL to the article.

**urlToImage**
*string*
The URL to a relevant image for the article.

**publishedAt**
*string*
The date and time that the article was published, in UTC (+000)

**content**
*string*
The unformatted content of the article, where available. This is truncated to 260 chars for Developer plan users.
